﻿using Python.Included;
using Python.Runtime;

Console.WriteLine("Install Python ...");
await Installer.SetupPython();

Console.WriteLine("Install Python Packages ...");
await Installer.InstallWheel(typeof(Program).Assembly, "numpy-1.21.5-cp37-cp37m-win_amd64.whl");
await Installer.InstallWheel(typeof(Program).Assembly, "scipy-1.7.3-cp37-cp37m-win_amd64.whl");


Console.WriteLine("Init Python Engine ...");
PythonEngine.Initialize();

Console.WriteLine("Add scripts to python path ...");
dynamic sys = PythonEngine.ImportModule("sys");
sys.path.append(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "scripts"));
Console.WriteLine($"Python pyth: {sys.path}");

dynamic version = PythonEngine.ImportModule("version");
Console.WriteLine($"Python version: {version.get_version()}");

dynamic usenumpy = PythonEngine.ImportModule("usenumpy");
Console.WriteLine($"Numpy version: {usenumpy.get_numpy_version()}");

dynamic usescipy = PythonEngine.ImportModule("usescipy");
Console.WriteLine($"Scipy version: {usescipy.get_scipy_version()}");


int a = 1;
int b = 2;
int c = add(a,b);
Console.WriteLine($"{a} + {b} = {c}");

int add(int a, int b) 
{
    dynamic usenumpy = PythonEngine.ImportModule("usenumpy");
    return usenumpy.add(a,b);
}