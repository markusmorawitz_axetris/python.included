﻿import scipy

def get_scipy_version():
    return scipy.__version__